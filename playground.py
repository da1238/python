
from datetime import date

def textualDate():
    today = date.today()
    d2 = today.strftime("%B %d, %Y")
    return d2


def getInitials(fullname):
    initials = ''
    namesplit = fullname.split(' ')
    for name in namesplit:
        initials += name[0]
    return initials

    